import { Holiday } from '../holiday';
import { Component, OnInit, ViewChild } from '@angular/core';

import { HOLIDAYS } from '../mock-holidays';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { HolidayService } from '../holiday.service';

@Component({
  selector: 'app-holidays-management',
  templateUrl: './holidays-management.component.html',
  styleUrls: ['./holidays-management.component.css']
})
export class HolidaysManagementComponent implements OnInit {

  editedHoliday = new Holiday();
  displayedColumns: string[] = ['name', 'date', 'actions'];
  holidaysDataSource = new MatTableDataSource<Holiday>();

  disableAddHolidayButton = false;
  disableValidateHolidayFormButton = false;
  hideHolidayForm = true;
  hideHolidaysLoadingProgressBar = true;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private holidaysService: HolidayService) { }


  ngOnInit() {
    this.holidaysDataSource.paginator = this.paginator;
    this.holidaysDataSource.sort = this.sort;
    this.loadAllHolidays();
  }

  onAddHolidayClick(): void {
    this.hideHolidayForm = false;
    this.disableAddHolidayButton = true;
  }

  onEditHolidayClick(holiday): void {
    this.editedHoliday = holiday;
    if (this.hideHolidayForm) {
      this.hideHolidayForm = false;
      this.disableAddHolidayButton = true;
    }
  }

  /**
  * Async method that deletes a holiday
  */
  async onDeleteHolidayClick(holiday) {
    this.hideHolidaysLoadingProgressBar = false;
    try {
      await this.holidaysService.deleteHoliday(holiday);
      const idx = this.holidaysDataSource.data.indexOf(holiday);
      const data = this.holidaysDataSource.data;
      data.splice(idx, 1);
      this.holidaysDataSource.data = data;
    } catch (error) {
      console.log(error);
    }
    this.hideHolidaysLoadingProgressBar = true;
  }

  /**
   * Async method that fetches all holidays and display them
   */
  async loadAllHolidays() {
    this.hideHolidaysLoadingProgressBar = false;
    try {
      this.holidaysDataSource.data = await this.holidaysService.getAllHolidays();
    } catch (error) {
      console.log(error);
    }
    this.hideHolidaysLoadingProgressBar = true;
  }


  /**
   * Async method that saves/updates a holiday
   */
  async onSaveHoliday($event) {
    this.hideHolidaysLoadingProgressBar = false;
    this.disableValidateHolidayFormButton = true;
    const data = this.holidaysDataSource.data;
    try {
      $event = await this.holidaysService.saveHoliday($event);
      // TODO is it the right way/place to do such a thing? Shouldn't we just reload both lists?
      if (!data.includes($event)) {
        data.push($event);
      }
      this.holidaysDataSource.data = data;
    } catch (e) {
      console.log(e);
    }
    this.hideHolidayForm = true;
    this.hideHolidaysLoadingProgressBar = true;
    this.editedHoliday = new Holiday();
    this.disableAddHolidayButton = false;
    this.disableValidateHolidayFormButton = false;
  }
}

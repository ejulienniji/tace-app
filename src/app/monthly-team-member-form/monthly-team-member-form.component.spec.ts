import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthlyTeamMemberFormComponent } from './monthly-team-member-form.component';

describe('MonthlyTeamMemberFormComponent', () => {
  let component: MonthlyTeamMemberFormComponent;
  let fixture: ComponentFixture<MonthlyTeamMemberFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonthlyTeamMemberFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthlyTeamMemberFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

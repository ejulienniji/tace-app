import { MonthlyTeamMember } from '../monthlyteammember';
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { HolidayService } from '../holiday.service';

@Component({
  selector: 'app-monthly-team-member-form',
  templateUrl: './monthly-team-member-form.component.html',
  styleUrls: ['./monthly-team-member-form.component.css']
})
export class MonthlyTeamMemberFormComponent implements OnInit {

  @Input() monthlyTeamMember: MonthlyTeamMember;
  @Input() month: number;
  @Input() year: number;
  @Output() validateClickEvent = new EventEmitter<MonthlyTeamMember>();

  businessDaysForTeamMember: number;

  invalidTace: boolean = false;

  constructor(private holidayService: HolidayService) {
  }

  ngOnInit() {
    this.businessDaysForTeamMember = this.holidayService.getBusinessDaysForTeamMember(this.monthlyTeamMember.teamMember.entryDate,
      this.monthlyTeamMember.teamMember.exitDate, this.month, this.year);
    this.monthlyTeamMember.producedDays = this.monthlyTeamMember.producedDays ? this.monthlyTeamMember.producedDays : this.businessDaysForTeamMember;
    this.monthlyTeamMember.vacations = this.monthlyTeamMember.vacations ? this.monthlyTeamMember.vacations : 0;
    this.monthlyTeamMember.unproducedDays = this.monthlyTeamMember.unproducedDays ? this.monthlyTeamMember.unproducedDays : 0;
  }

  onValidate(monthlyTeamMember: MonthlyTeamMember) {
    this.invalidTace = false;
    if (monthlyTeamMember.producedDays + monthlyTeamMember.unproducedDays + monthlyTeamMember.vacations
      !== this.businessDaysForTeamMember) {
      this.invalidTace = true;
    } else {
      this.validateClickEvent.emit(monthlyTeamMember);
    }
  }

}

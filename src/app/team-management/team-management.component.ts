import {TeamService} from '../team.service';
import {Component, OnInit} from '@angular/core';

import {TeamMember} from '../teammember';


@Component({
  selector: 'app-team-management',
  templateUrl: './team-management.component.html',
  styleUrls: ['./team-management.component.css']
})
export class TeamManagementComponent implements OnInit {

  team: TeamMember[] = [];
  pastteam: TeamMember[] = [];
  editedTeamMember = new TeamMember();

  hideTeamMemberForm = true;
  hideTeamLoadingProgressBar = true;
  hidePastTeamLoadingProgressBar = true;
  disableAddTeamMemberButton = false;

  constructor(private teamService: TeamService) {}

  ngOnInit() {
    this.loadCurrentTeam();
  }

  /**
   * UI modifications when user clicks the add new team member button
   */
  onAddTeamMemberClick(): void {
    this.hideTeamMemberForm = false;
    this.disableAddTeamMemberButton = true;
  }

  /**
 * UI modifications when user clicks edit team member button
 */
  onEditTeamMemberClick($event): void {
    this.disableAddTeamMemberButton = true;
    this.editedTeamMember = $event;
    if (this.hideTeamMemberForm) {
      this.hideTeamMemberForm = false;
    }
  }

  /**
   * When user navigates through tabs we have to load data
   */
  onSelectedTabChange($event): void {
    if ($event.index === 0) {
      this.loadCurrentTeam();
    } else if ($event.index === 1) {
      this.loadPastTeam();
    }
  }

  /**
   * Async method that saves/updates the team member
   */
  async onSaveTeamMember($event) {
    this.hideTeamLoadingProgressBar = false;
    try {
      $event = await this.teamService.saveTeamMember($event);
      // TODO is it the right way/place to do such a thing? Shouldn't we just reload both lists?
      if (this.team.includes($event)) {
        if ($event.exitDate) {
          const idx = this.team.indexOf($event);
          this.team.splice(idx, 1);
        }
        this.pastteam.push($event);
      } else {
        this.team.push($event);
      }
    } catch (e) {
      console.log(e);
    }
    this.hideTeamMemberForm = true;
    this.hideTeamLoadingProgressBar = true;
    this.editedTeamMember = new TeamMember();
    this.disableAddTeamMemberButton = false;
  }

  /**
   * Async method that deletes the team member
   */
  async onDeleteTeamMember($event) {
    this.hideTeamLoadingProgressBar = false;
    this.hidePastTeamLoadingProgressBar = false;
    try {
      await this.teamService.deleteTeamMember($event);
      // TODO is it the right way/place to do such a thing? Shouldn't we just reload both lists?
      if (this.team.includes($event)) {
        const idx = this.team.indexOf($event);
        this.team.splice(idx, 1);
      } else if (this.pastteam.includes($event)) {
        const idx = this.pastteam.indexOf($event);
        this.pastteam.splice(idx, 1);
      }
    } catch (e) {
      console.log(e);
    }
    this.hideTeamLoadingProgressBar = true;
    this.hidePastTeamLoadingProgressBar = true;
  }

  /**
   * Async method that fetches data for the current team
   */
  async loadCurrentTeam() {
    this.hideTeamLoadingProgressBar = false;
    try {
      this.team = await this.teamService.getCurrentTeam();
    } catch (e) {
      console.log(e);
    }
    this.hideTeamLoadingProgressBar = true;
  }

  /**
   * Async method that fetches data for the team members that are no longer in
   */
  async loadPastTeam() {
    this.hidePastTeamLoadingProgressBar = false;
    try {
      this.pastteam = await this.teamService.getPastTeam();
    } catch (e) {
      console.log(e);
    }
    this.hidePastTeamLoadingProgressBar = true;
  }
}

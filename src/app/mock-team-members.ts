import { TeamMember } from './teammember';

export const TEAM: TeamMember[] = [
  { index: 1, fullname: 'Damien Metais', entryDate: new Date('2017-09-25'), exitDate: null },
  { index: 2, fullname: 'Perrine Caekaert', entryDate: new Date('2018-06-05'), exitDate: null },
  { index: 3, fullname: 'Victor Antonelli', entryDate: new Date('2018-06-05'), exitDate: null },
  { index: 4, fullname: 'Jessica Brosset', entryDate: new Date('2018-04-01'), exitDate: null },
  { index: 5, fullname: 'Damien Boyer', entryDate: new Date('2018-06-05'), exitDate: null },
  { index: 6, fullname: 'Maxime De Chalendar', entryDate: new Date('2018-02-05'), exitDate: new Date('2018-06-29') },
  { index: 7, fullname: 'Guillaume Faucon', entryDate: new Date('2018-05-28'), exitDate: null },
  { index: 8, fullname: 'Emmanuel Julien', entryDate: new Date('2018-06-04'), exitDate: null }
];

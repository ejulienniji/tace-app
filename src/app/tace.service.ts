import { Injectable } from '@angular/core';
import { Month } from './month';
import { MonthlyTeamMember } from './monthlyteammember';
import { MONTHS } from './mock-months';
import { TeamService } from './team.service';
import { HolidayService } from './holiday.service';
import { TeamMember } from './teammember';

@Injectable({
  providedIn: 'root'
})
export class TaceService {

  constructor(private holidaysService: HolidayService, private teamService: TeamService) { }

  loadMonthlyTeam(year: number, month: number): Promise<MonthlyTeamMember[]> {
    return new Promise<MonthlyTeamMember[]>(async resolve => {
      let monthlyTeam: MonthlyTeamMember[] = [];
      MONTHS.forEach(monthlyTace => {
        if (monthlyTace.id === month && monthlyTace.year === year) {
          monthlyTeam = monthlyTace.monthlyTeam;
        }
      });
      if (monthlyTeam.length === 0) {
        let mTeam = await this.teamService.getTeamForMonth(month, year);
        mTeam.map(element => {
          let mTeamMember = new MonthlyTeamMember();
          mTeamMember.teamMember = element;
          monthlyTeam.push(mTeamMember)
        });
      }
      setTimeout(() => {
        resolve(monthlyTeam);
      }, 2000);
    });
  }

  loadAllMonths(): Promise<Month[]> {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(MONTHS);
      }, 2000);
    });
  }

  saveMonth(monthlyTeam: MonthlyTeamMember[], month: number, year: number): Promise<Month> {
    let taceSum = 0;
    let tace = 0;
    let newMonth = new Month();
    newMonth.id = month;
    newMonth.year = year;
    newMonth.monthlyTeam = monthlyTeam;
    if (monthlyTeam.length > 0) {
      monthlyTeam.forEach(mtm => {
        taceSum += mtm.getTACEForTeamMember(
          this.holidaysService.getBusinessDaysForTeamMember(mtm.teamMember.entryDate, mtm.teamMember.exitDate, month, year));
      });
      tace = taceSum / monthlyTeam.length;
    }
    newMonth.tace = tace;
    newMonth.holidays = this.holidaysService.getHolidaysForMonth(month, year);
    return new Promise(resolve => {
      setTimeout(() => {
        let idx = MONTHS.findIndex(function (value, index, obj) {
          return value.id === month && value.year === year;
        });
        if (idx !== -1) {
          MONTHS[idx] = newMonth;
        } else {
          MONTHS.push(newMonth);
        }
        resolve(newMonth);
      }, 2000);
    });
  }
}

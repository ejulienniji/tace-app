import { TestBed, inject } from '@angular/core/testing';

import { TaceService } from './tace.service';

describe('TaceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TaceService]
    });
  });

  it('should be created', inject([TaceService], (service: TaceService) => {
    expect(service).toBeTruthy();
  }));
});

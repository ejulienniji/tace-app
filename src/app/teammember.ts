export class TeamMember {
  index: number;
  fullname: string;
  entryDate: Date;
  exitDate: Date;
}

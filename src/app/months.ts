export class DisplayMonth {
  id: number;
  name: string;
}

export const DISPLAYMONTHS: DisplayMonth[] = [
  {id: 0, name: 'Janvier'},
  {id: 1, name: 'Février'},
  {id: 2, name: 'Mars'},
  {id: 3, name: 'Avril'},
  {id: 4, name: 'Mai'},
  {id: 5, name: 'Juin'},
  {id: 6, name: 'Juillet'},
  {id: 7, name: 'Août'},
  {id: 8, name: 'Septembre'},
  {id: 9, name: 'Octobre'},
  {id: 10, name: 'Novembre'},
  {id: 11, name: 'Décembre'}
];

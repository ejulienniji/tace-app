import { MonthlyTeamMember } from '../monthlyteammember';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { DISPLAYMONTHS, DisplayMonth } from '../months';
import { TaceService } from '../tace.service';
import { HolidayService } from '../holiday.service';

@Component({
  selector: 'app-tace-computing',
  templateUrl: './tace-computing.component.html',
  styleUrls: ['./tace-computing.component.css']
})
export class TaceComputingComponent implements OnInit {


  currentYear: number = (new Date()).getFullYear();
  years = [this.currentYear - 1, this.currentYear, this.currentYear + 1];

  months = DISPLAYMONTHS;

  providedMonthlyTeam: MonthlyTeamMember[] = [];
  unprovidedMonthlyTeam: MonthlyTeamMember[];

  selectedYear: number;
  selectedMonth: DisplayMonth;

  disableMonthChangerButton = true;
  disableYearAndMonthSelectedButton = false;
  disableTerminateMonthButton = true;
  hideMonthlyTeamLoadingProgressBar = true;
  hideLoadingSpinner = true;

  constructor(private route: ActivatedRoute, private router: Router,
    private taceService: TaceService, private holidayService: HolidayService) { }

  ngOnInit() {
    if (this.route.snapshot.params && this.route.snapshot.params.year && this.route.snapshot.params.month) {
      this.selectedYear = +this.route.snapshot.params.year;
      this.selectedMonth = this.months[this.route.snapshot.params.month];
      this.loadAndDisplayMonthlyTeam(this.selectedYear, this.selectedMonth.id);
    } else {
      this.selectedYear = this.currentYear;
      this.selectedMonth = this.months[(new Date()).getMonth()];
    }
  }

  onUnselectYearAndMonth(year: number, month: number): void {
    this.router.navigate(['/tace-computing', year, month]);
    this.loadAndDisplayMonthlyTeam(year, month);
  }

  onYearAndMonthSelected(year: number, month: number): void {
    this.router.navigate(['/tace-computing', year, month]);
    this.loadAndDisplayMonthlyTeam(year, month);
  }

  async onTerminateMonthClick() {
    try {
      this.hideLoadingSpinner = false;
      await this.taceService.saveMonth(this.providedMonthlyTeam, this.selectedMonth.id, this.selectedYear);
      this.hideLoadingSpinner = true;
      this.router.navigate(['tace']);
    } catch (e) {
      console.log(e);
    }
  }

  onEditMonthlyTeamMemberClick($event: MonthlyTeamMember) {
    const idx = this.providedMonthlyTeam.indexOf($event);
    this.providedMonthlyTeam.splice(idx, 1);
    this.unprovidedMonthlyTeam.push($event);
    this.disableTerminateMonthButton = !(this.providedMonthlyTeam && this.providedMonthlyTeam.length > 0
      && this.unprovidedMonthlyTeam && this.unprovidedMonthlyTeam.length === 0);
  }

  onValidateTeamMemberClick($event: MonthlyTeamMember) {
    const idx = this.unprovidedMonthlyTeam.indexOf($event);
    this.unprovidedMonthlyTeam.splice(idx, 1);
    this.providedMonthlyTeam.push($event);
    this.disableTerminateMonthButton = !(this.providedMonthlyTeam && this.providedMonthlyTeam.length > 0
      && this.unprovidedMonthlyTeam && this.unprovidedMonthlyTeam.length === 0);
  }

  async loadAndDisplayMonthlyTeam(year: number, month: number) {
    this.hideMonthlyTeamLoadingProgressBar = false;
    this.unprovidedMonthlyTeam = await this.taceService.loadMonthlyTeam(year, month);
    this.disableMonthChangerButton = false;
    this.disableYearAndMonthSelectedButton = true;
    this.hideMonthlyTeamLoadingProgressBar = true;
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaceComputingComponent } from './tace-computing.component';

describe('TaceComputingComponent', () => {
  let component: TaceComputingComponent;
  let fixture: ComponentFixture<TaceComputingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaceComputingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaceComputingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Holiday} from '../holiday';
import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-holiday-form',
  templateUrl: './holiday-form.component.html',
  styleUrls: ['./holiday-form.component.css']
})
export class HolidayFormComponent implements OnInit {

  @Input() holiday: Holiday;
  @Input() disableValidateHolidayFormBtn: boolean;
  @Output() saveEvent = new EventEmitter<void>();

  constructor() {}

  ngOnInit() {
    if (!this.holiday) {
      this.holiday = new Holiday();
    }
  }

  onSaveHolidayClick(holiday) {
    this.saveEvent.emit(holiday);
  }

}

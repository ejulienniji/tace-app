import {HolidaysManagementComponent} from './holidays-management/holidays-management.component';
import {HomeComponent} from './home/home.component';
import {TaceComputingComponent} from './tace-computing/tace-computing.component';
import {TaceManagementComponent} from './tace-management/tace-management.component';
import {TeamManagementComponent} from './team-management/team-management.component';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'holidays', component: HolidaysManagementComponent},
  {path: 'home', component: HomeComponent},
  {path: 'tace', component: TaceManagementComponent},
  {path: 'tace-computing', component: TaceComputingComponent},
  {path: 'tace-computing/:year/:month', component: TaceComputingComponent},
  {path: 'team', component: TeamManagementComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})

export class AppRoutingModule {}

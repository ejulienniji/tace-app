import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaceManagementComponent } from './tace-management.component';

describe('TaceManagementComponent', () => {
  let component: TaceManagementComponent;
  let fixture: ComponentFixture<TaceManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaceManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaceManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

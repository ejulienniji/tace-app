import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';

import {Month} from '../month';
import {SelectionModel} from '@angular/cdk/collections';
import {MatTableDataSource, MatPaginator, MatSort} from '@angular/material';

import {DISPLAYMONTHS} from '../months';
import { TaceService } from '../tace.service';

@Component({
  selector: 'app-tace-management',
  templateUrl: './tace-management.component.html',
  styleUrls: ['./tace-management.component.css']
})
export class TaceManagementComponent implements OnInit {


  displayedColumns: string[] = ['select', 'name', 'tace'];
  displayMonths = DISPLAYMONTHS;
  taceDataSource = new MatTableDataSource<Month>();
  taceSelection = new SelectionModel<Month>(true, []);

  disableAddHolidayButton = false;
  hideHolidayForm = true;
  hideTaceLoadingProgressBar = true;

  editButtonLabel = 'Renseigner un nouveau mois';
  disableEditButton = false;
  hideExportButton = true;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private router: Router, private taceService: TaceService) {}

  ngOnInit() {
    this.taceDataSource.paginator = this.paginator;
    this.taceDataSource.sort = this.sort;
    this.loadAndDisplayAllMonths();
  }

  onEditMonthClick(): void {
    if (this.taceSelection.hasValue() && this.taceSelection.selected.length === 1) {
      const year = this.taceSelection.selected[0].year;
      const month = this.taceSelection.selected[0].id;
      this.router.navigate(['/tace-computing', year, month]);
    } else if (!this.taceSelection.hasValue()) {
      this.router.navigate(['/tace-computing']);
    }
  }

  onCheckBoxChange($event, row): void {
    this.hideExportButton = true;
    this.disableEditButton = false;
    if (this.taceSelection.hasValue()) {
      this.hideExportButton = false;
      if (this.taceSelection.selected.length > 1) {
        this.disableEditButton = true;

      } else {
        this.editButtonLabel = 'Editer le mois sélectionné';
      }
    } else {
      this.editButtonLabel = 'Renseigner un nouveau mois';
    }
  }

  async loadAndDisplayAllMonths() {
    this.hideTaceLoadingProgressBar = false;
    try {
      this.taceDataSource.data = await this.taceService.loadAllMonths();
    } catch (error) {
      console.log(error);
    }
    this.hideTaceLoadingProgressBar = true;
  }
}

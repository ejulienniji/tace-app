import { Injectable } from '@angular/core';
import { Holiday } from './holiday';

import { HOLIDAYS } from './mock-holidays';

@Injectable({
    providedIn: 'root'
})
export class HolidayService {

    constructor() { }


    /**
     * Returns the array of all holidays
     */
    getAllHolidays(): Promise<Holiday[]> {
        return new Promise(resolve => {
            setTimeout(() => {
                resolve(HOLIDAYS);
            }, 2000);
        });
    }

    /**
     * Saves or updates a team member
     */
    saveHoliday(holiday: Holiday): Promise<Holiday> {
        this.verifyHoliday(holiday);
        return new Promise(resolve => {
            let idx = HOLIDAYS.findIndex(function (value, index, obj) {
                return value.date.getTime() === holiday.date.getTime() && value.name === holiday.name;
            });
            if (idx !== -1) {
                HOLIDAYS[idx]=holiday;
            } else {
                HOLIDAYS.push(holiday);
            }
            setTimeout(() => {
                resolve(holiday);
            }, 2000);
        });
    }

    /**
     * Deletes a holiday
     * @param holiday 
     */
    deleteHoliday(holiday: Holiday): Promise<boolean> {
        return new Promise(resolve => {
            let idx = HOLIDAYS.findIndex(function (value, index, obj) {
                return value.date.getTime() === holiday.date.getTime() && value.name === holiday.name;
            });
            if (idx !== -1) {
                HOLIDAYS.splice(idx, 1);
            }
            setTimeout(() => {
                resolve(true);
            }, 2000);
        });
    }

    /**
     * 
     * @param holiday 
     */
    private verifyHoliday(holiday: Holiday): void {
        if (!holiday) {
            throw new Error('Invalid holiday: undefined');
        }
        if (!holiday.name || holiday.name.trim() === '') {
            throw new Error('Invalid holiday: name');
        }
        if (!holiday.date) {
            throw new Error('Invalid holiday: date');
        }
    }

    getBusinessDaysForTeamMember(entryDate: Date, exitDate: Date, month: number, year: number): number {
        let totalDays = 0;
        let startDate = new Date();
        startDate.setDate(1);
        startDate.setMonth(month);
        startDate.setFullYear(year);
        if (entryDate.getFullYear() === year && entryDate.getMonth() === month) {
            startDate.setDate(entryDate.getDate());
        }
        // jours feriés à exclure
        let includedHolidays = 0;
        HOLIDAYS.forEach(holiday => {
            if (holiday.date.getFullYear() === year && holiday.date.getMonth() === month
                && holiday.date.getDate() >= startDate.getDate()
                && ((exitCond && holiday.date.getDate() <= exitDate.getDate()) || !exitCond)
                && 0 < holiday.date.getDay() && holiday.date.getDay() < 6) {
                includedHolidays++;
            }
        });
        // la date de sortie existe et est dans le mois et l'annee demandee
        let exitCond = exitDate && exitDate.getFullYear() === year && exitDate.getMonth() === month;
        while (startDate.getMonth() < month + 1 && (!exitDate || exitCond && startDate.getDate() < exitDate.getDate())) {
            if (0 < startDate.getDay() && startDate.getDay() < 6) {
                totalDays++;
            }
            startDate = new Date(startDate.setTime(startDate.getTime() + 86400000));
        }
        return totalDays - includedHolidays;
    }

    getHolidaysForMonth(month: number, year: number): Holiday[] {
        let result: Holiday[] = [];
        HOLIDAYS.forEach(h => {
            if (h.date.getFullYear() === year && h.date.getMonth() === month) {
                result.push(h);
            }
        });
        return result;
    }


}

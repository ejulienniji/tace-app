import {Component, OnInit, Input, EventEmitter, Output} from '@angular/core';

import {TeamMember} from '../teammember';

@Component({
  selector: 'app-team-member-form',
  templateUrl: './team-member-form.component.html',
  styleUrls: ['./team-member-form.component.css']
})
export class TeamMemberFormComponent implements OnInit {

  @Input() teamMember: TeamMember;
  @Output() saveClickEvent = new EventEmitter<TeamMember>();

  constructor() {}

  ngOnInit() {
    if (!this.teamMember) {
      this.teamMember = new TeamMember();
    }
  }

  onSaveTeamMemberClick(teamMember): void {
    this.saveClickEvent.emit(this.teamMember);
  }

}

import {Month} from './month';
import {TeamMember} from './teammember';

export class MonthlyTeamMember {
  teamMember: TeamMember;
  producedDays: number;
  vacations: number;
  unproducedDays: number;

  constructor() {}

  public getTACEForTeamMember(businessDaysForTeamMember: number): number {
    let result = 100;
    if ((businessDaysForTeamMember - this.vacations) !== 0) {
      result = 100 * this.producedDays / (businessDaysForTeamMember - this.vacations);
    }
    return result;
  }
}

import { } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {
  MatButtonModule, MatCardModule, MatCheckboxModule,
  MatDatepickerModule, MatFormFieldModule, MatIconModule,
  MatInputModule, MatListModule, MatNativeDateModule,
  MatOptionModule, MatPaginatorModule, MatProgressBarModule,
  MatProgressSpinnerModule, MatSelectModule, MatSidenavModule,
  MatSortModule, MatTableModule, MatTabsModule,
  MatToolbarModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { TeamMemberFormComponent } from './team-member-form/team-member-form.component';
import { TeamMemberCardComponent } from './team-member-card/team-member-card.component';
import { MainSidenavComponent } from './main-sidenav/main-sidenav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { TeamManagementComponent } from './team-management/team-management.component';
import { HolidaysManagementComponent } from './holidays-management/holidays-management.component';
import { HolidayFormComponent } from './holiday-form/holiday-form.component';
import { TaceManagementComponent } from './tace-management/tace-management.component';
import { TaceComputingComponent } from './tace-computing/tace-computing.component';
import { MonthlyTeamMemberFormComponent } from './monthly-team-member-form/monthly-team-member-form.component';
import { MonthlyTeamMemberCardComponent } from './monthly-team-member-card/monthly-team-member-card.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TeamMemberFormComponent,
    TeamMemberCardComponent,
    MainSidenavComponent,
    TeamManagementComponent,
    HolidaysManagementComponent,
    HolidayFormComponent,
    TaceManagementComponent,
    TaceComputingComponent,
    MonthlyTeamMemberFormComponent,
    MonthlyTeamMemberCardComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    NgbModule.forRoot(),
    LayoutModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatOptionModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatSidenavModule,
    MatSelectModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

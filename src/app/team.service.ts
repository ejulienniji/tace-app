import { Injectable } from '@angular/core';
import { TeamMember } from './teammember';

import { TEAM } from './mock-team-members';


@Injectable({
  providedIn: 'root'
})
export class TeamService {

  constructor() { }

  /**
   * Returns the array of TeamMember who have no exitDate
   */
  getCurrentTeam(): Promise<TeamMember[]> {
    let year = (new Date()).getFullYear();
    let month = (new Date()).getMonth();
    return this.getTeamForMonth(month, year);
  }

  getTeamForMonth(month: number, year: number): Promise<TeamMember[]> {
    return new Promise(resolve => {
      let result: TeamMember[] = [];
      TEAM.forEach(tm => {
        if (tm.entryDate.getFullYear() < year || tm.entryDate.getFullYear() === year && tm.entryDate.getMonth() <= month) {
          if (!tm.exitDate || tm.exitDate.getFullYear() > year || tm.exitDate.getFullYear() === year && tm.exitDate.getMonth() >= month) {
            result.push(tm);
          }
        }
      });
      setTimeout(() => {
        resolve(result);
      }, 2000);
    });
  }

  /**
   * Returns the array of TeamMember with an exitDate
   */
  getPastTeam(): Promise<TeamMember[]> {
    return new Promise(resolve => {
      let year = (new Date()).getFullYear();
      let month = (new Date()).getMonth();
      let result: TeamMember[] = [];
      TEAM.forEach(tm => {
        if (tm.exitDate && (tm.exitDate.getFullYear() < year || tm.exitDate.getFullYear() === year && tm.exitDate.getMonth() < month)) {
          result.push(tm);
        }
      });
      setTimeout(() => {
        resolve(result);
      }, 2000);
    });
  }

  /**
   * Saves or updates a team member
   */
  saveTeamMember(teamMember: TeamMember): Promise<TeamMember> {
    if (!this.verifyTeamMember(teamMember)) {
      throw new Error('Invalid TeamMember: date issue');
    }
    return new Promise(resolve => {
      if (teamMember.index) {
        let index = TEAM.findIndex(function (value, index, obj) {
          return value.index === teamMember.index;
        });
        TEAM[index] = teamMember;
      } else {
        teamMember.index = TEAM[TEAM.length - 1].index + 1;
        TEAM.push(teamMember);
      }
      setTimeout(() => {
        resolve(teamMember);
      }, 2000);
    });
  }

  /***
   * deletes a team member
   */
  deleteTeamMember(teamMember: TeamMember): Promise<boolean> {
    return new Promise(resolve => {
      if (teamMember.index) {
        let index = TEAM.findIndex(function (value, index, obj) {
          return value.index === teamMember.index;
        });
        TEAM.splice(index, 1);
      }
      setTimeout(() => {
        resolve(true);
      }, 2000);
    });
  }

  /**
   * Checks data validity
   */
  private verifyTeamMember(teamMember: TeamMember): boolean {
    return (teamMember.fullname.trim() !== "" && teamMember.entryDate &&
      (!teamMember.exitDate || teamMember.exitDate.getTime() > teamMember.entryDate.getTime()));
  }
}

import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

import {TeamMember} from '../teammember';

@Component({
  selector: 'app-team-member-card',
  templateUrl: './team-member-card.component.html',
  styleUrls: ['./team-member-card.component.css']
})
export class TeamMemberCardComponent implements OnInit {

  @Input() teamMember: TeamMember;
  @Output() editClickEvent = new EventEmitter<TeamMember>();
  @Output() deleteClickEvent = new EventEmitter<TeamMember>();

  constructor() {}

  ngOnInit() {
  }

  onEditTeamMemberButtonClicked() {
    this.editClickEvent.emit(this.teamMember);
  }

  onDeleteTeamMemberButtonClicked() {
    this.deleteClickEvent.emit(this.teamMember);
  }
}

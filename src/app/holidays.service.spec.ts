import { TestBed, inject } from '@angular/core/testing';

import { HolidayService } from './holiday.service';

describe('HolidaysService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HolidayService]
    });
  });

  it('should be created', inject([HolidayService], (service: HolidayService) => {
    expect(service).toBeTruthy();
  }));
});

import {Holiday} from './holiday';
import { MonthlyTeamMember } from './monthlyteammember';
export class Month {
  id: number;
  year: number;
  holidays: Holiday[];
  monthlyTeam: MonthlyTeamMember[];
  tace: number;
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthlyTeamMemberCardComponent } from './monthly-team-member-card.component';

describe('MonthlyTeamMemberCardComponent', () => {
  let component: MonthlyTeamMemberCardComponent;
  let fixture: ComponentFixture<MonthlyTeamMemberCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonthlyTeamMemberCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthlyTeamMemberCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

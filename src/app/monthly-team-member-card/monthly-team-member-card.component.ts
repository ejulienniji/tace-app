import { MonthlyTeamMember } from '../monthlyteammember';
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { HolidayService } from '../holiday.service';

@Component({
  selector: 'app-monthly-team-member-card',
  templateUrl: './monthly-team-member-card.component.html',
  styleUrls: ['./monthly-team-member-card.component.css']
})
export class MonthlyTeamMemberCardComponent implements OnInit {

  @Input() monthlyTeamMember: MonthlyTeamMember;
  @Input() month: number;
  @Input() year: number;
  @Output() editClickEvent = new EventEmitter<MonthlyTeamMember>();

  businessDaysForTeamMember: number;

  constructor(private holidayService: HolidayService) { }

  ngOnInit() {
    this.businessDaysForTeamMember = this.holidayService.getBusinessDaysForTeamMember(this.monthlyTeamMember.teamMember.entryDate,
      this.monthlyTeamMember.teamMember.exitDate, this.month, this.year);
  }

  onEdit(monthlyTeamMember: MonthlyTeamMember) {
    this.editClickEvent.emit(monthlyTeamMember);
  }

}
